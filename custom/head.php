<!-- A favicon (the image that appears on the site tab) -->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/><!-- This line is for responsive site -->
<meta name="description" content="This is the Highest paying autofaucet. This faucet never stops and send your earnings in every Intervals for free to boost your free earnings." />
<meta name="keywords" content="Auto Faucet, AutoFaucet, no hidden mining, Auto claim Faucet, floodgate,  faucet, dutchycorp" />
<!-- Some CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.2.1/font-awesome-animation.min.css" />

<link rel="stylesheet" type="text/css" href="/website.css"/>
<link rel="stylesheet" type="text/css" href="/custom/materialize.min.css"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="custom/modern_navbar.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<!-- 
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" media="screen,projection">-->
<!-- No need to modify bellow code. Use https://www.favicon-generator.org/ to generate favicon ad replace generated images inside /favicon folder. -->
<link rel="apple-touch-icon" sizes="57x57" href="https://i.ibb.co/N2SSRZx/logo-b-w1-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="https://i.ibb.co/0YtXh5k/logo-b-w1-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://i.ibb.co/g3rN5SN/logo-b-w1-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="https://i.ibb.co/rxfc8cH/logo-b-w1-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://i.ibb.co/Y20K3LZ/logo-b-w1-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="https://i.ibb.co/SX9G201/logo-b-w1-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="https://i.ibb.co/xY3sq6Z/logo-b-w1-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="https://i.ibb.co/BCBk37r/logo-b-w1-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="https://i.ibb.co/W3KFrww/logo-b-w1-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="https://image.ibb.co/efxPCf/logo-b-w1-192px2.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://image.ibb.co/dWoNXf/logo-gris-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="https://i.ibb.co/pZSrB04/logo-b-w1-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://i.ibb.co/c84WxYR/logo-gris-16x16.png">
<link rel="manifest" href="/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="https://i.ibb.co/xY3sq6Z/logo-b-w1-144x144.png">
<meta name="theme-color" content="#ffffff">

<!-- Some CSS -->
<link rel="stylesheet" type="text/css" href="/website.css"/>

<!-- IDL (Internet Defense League) -->
<script type="text/javascript">window._idl={};_idl.variant="modal";(function(){var idl=document.createElement('script');idl.async=true;idl.src='https://members.internetdefenseleague.org/include/?url='+(_idl.url||'')+'&campaign='+(_idl.campaign||'')+'&variant='+(_idl.variant||'modal');document.getElementsByTagName('body')[0].appendChild(idl);})();</script>

<!-- Ads Rotation -->
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/custom/ads_rotate.php'; ?>

<!-- Google Analytics -->
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php'; ?>
<?php if ($cfg_enable_google_analytics) {
  echo '<script async src="https://www.googletagmanager.com/gtag/js?id=' . $cfg_ga_ID . '"></script>';
  echo '<script>';
  echo 'window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments);}';
  echo 'gtag(\'js\',new Date());';
  echo 'gtag(\'config\',\'' . $cfg_ga_ID . '\');';
  if (isset($user_hash) && !empty($user_hash))
    echo 'gtag(\'set\', {\'user_id\': \'' . $user_hash . '\'});';
  echo '</script>';
} ?>
